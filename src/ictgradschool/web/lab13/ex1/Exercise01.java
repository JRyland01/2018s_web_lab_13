package ictgradschool.web.lab13.ex1;

import javax.xml.transform.Result;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.*;
import java.util.Properties;

public class Exercise01 {
    public static void main(String[] args) {
        /* The following verifies that your JDBC driver is functioning. You may base your solution on this code */
        Properties dbProps = new Properties();

        try (FileInputStream fIn = new FileInputStream("mysql.properties")) {
            dbProps.load(fIn);
        } catch (IOException e) {
            e.printStackTrace();
        }
        MovieContent(dbProps);
    }


        // Set the database name to your database
        private static void MovieContent (Properties dbProps) {
            try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
                System.out.println("Connection successful");
                System.out.println("What is the title you are looking for?");
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                String s = br.readLine();
                try (PreparedStatement stmt = conn.prepareStatement("SELECT body FROM lab13_articles WHERE title LIKE ?")) {
                    s += "%";
                    stmt.setString(1, s);
                    try (ResultSet r = stmt.executeQuery()) {
                        if (r.getFetchSize() == 0) {
                            System.out.println("That name does not exist in this database, reconnecting...");
                            MovieContent(dbProps);

                        } else {
                            while (r.next()) {
                                String userString = r.getString(1);
                                System.out.println("Your movie content is:\n" + userString);
                            }
                        }
                    }


                }

                //Anything that requires the use of the connection should be in here...


            } catch (SQLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

