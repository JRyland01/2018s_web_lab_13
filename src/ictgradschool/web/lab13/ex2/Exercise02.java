package ictgradschool.web.lab13.ex2;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class Exercise02 {
    public static void main(String[] args) {
        /* The following verifies that your JDBC driver is functioning.*/
        Properties dbProps = new Properties();

        try (FileInputStream fIn = new FileInputStream("mysql.properties")) {
            dbProps.load(fIn);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)){
            System.out.println("Welcome to the film database!");
            choices(conn, dbProps);

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    public static void choices (Connection conn, Properties dbProps){
        System.out.println("Please select an option from the following:");
        System.out.println("1: Information by Actor");
        System.out.println("2: Information by Movie");
        System.out.println("3: Information by Genre");
        System.out.println("4: Exit");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        try {
            int userChoice = Integer.parseInt(br.readLine());
            switch (userChoice){
                case 1: informationByActor(conn, dbProps);
                    break;
                case 2: informationByMovie(conn, dbProps);
                    break;
                case 3: informationByGenre(conn, dbProps);
                    break;
                case 4: exitTime();
                    break;
                default:
                    System.out.println("Invalid Choice");
                    choices(conn, dbProps);
                    break;

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void informationByActor (Connection conn, Properties dbProps) throws IOException {
        System.out.println("Please enter the name of the actor you wish to get information about, or press enter to return to the menu.");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String actorName = br.readLine();
        if(actorName.isEmpty()){
            System.out.println("That name does not exist \n");
            choices(conn, dbProps);
        }else{
            try (PreparedStatement stmt = conn.prepareStatement("SELECT PF.film_title, PR.role_name FROM pfilms_actor AS PA, pfilms_participates_in AS PPI, pfilms_film AS PF, pfilms_role AS PR WHERE (PA.actor_fname LIKE ? OR PA.actor_lname LIKE ?) AND PA.actor_id = PPI.actor_id AND PF.film_id = PPI.film_id AND PR.role_id = PPI.role_id")){
                stmt.setString(1,actorName);
                stmt.setString(2,actorName);

                try (ResultSet r = stmt.executeQuery()){
                    List<String>filmName = new ArrayList<>();
                    List<String> roleName =new ArrayList<>();
                    while (r.next()){
                        filmName.add(r.getString(1));
                        roleName.add(r.getString(2));
                        }
                    System.out.println(actorName+" is listed as being involved in the following films: ");
                    System.out.println();
                    for (int i = 0; i <filmName.size();i++) {
                            System.out.println(filmName.get(i)+"("+roleName.get(i)+")");
                            }

                    }
                    System.out.println("\n");
                    choices(conn,dbProps);


            }
            catch (SQLException e){
                System.out.println("error");
            }
            }
        }

    public static void informationByMovie (Connection conn, Properties dbProps) {
        System.out.println("Please enter the name of the film you wish to get information about, or press enter to return to the previous menu.");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String movieName = null;
        try {
            movieName = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (movieName.isEmpty()) {
            System.out.println("That movie name does not exist in these archives \n");
            choices(conn, dbProps);
        } else {
            try (PreparedStatement stmt = conn.prepareStatement(" SELECT FA.actor_fname, FA.actor_lname, FRA.role_name FROM pfilms_actor AS FA, pfilms_role AS FRA, pfilms_film AS FF, pfilms_participates_in AS PPI WHERE FF.film_title LIKE ? AND FRA.role_id = PPI.role_id AND PPI.actor_id = FA.actor_id AND FF.film_id = PPI.film_id")){
            stmt.setString(1, movieName);
            try(ResultSet r = stmt.executeQuery()){
                List<String> fname = new ArrayList<>();
                List<String> lname = new ArrayList<>();
                List<String> roleName = new ArrayList<>();
                while(r.next()){
                    fname.add(r.getString(1));
                    lname.add(r.getString(2));
                    roleName.add(r.getString(3));
                }
                System.out.println("The film "+movieName+ " is a Action movie that features\n" +
                        "the following people:\n");
                for(int i = 0; i<fname.size();i++){
                    System.out.println(fname.get(i)+lname.get(i)+"("+roleName.get(i)+")");
                }
                System.out.println("\n");
                choices(conn,dbProps);
            }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
            public static void informationByGenre (Connection conn, Properties dbProps){
                System.out.println("Please enter the name of the genre you wish to get information about, or press enter to return to the previous menu");
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                String genreName = null;
                try {
                    genreName = br.readLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (genreName.isEmpty()) {
                    System.out.println("That genre does not exist in these archives \n");
                    choices(conn, dbProps);
                } else {
                    try (PreparedStatement stmt = conn.prepareStatement("SELECT film_title FROM pfilms_film AS FF WHERE genre_name LIKE ?")){
                        stmt.setString(1, genreName);
                        try(ResultSet r = stmt.executeQuery()){
                            List<String> movieNames = new ArrayList<>();
                            while(r.next()){
                                movieNames.add(r.getString(1));
                            }
                            System.out.println("The "+genreName +" genre includes the following films:\n");
                            for (int i = 0;i<movieNames.size();i++){
                                System.out.println(movieNames.get(i));
                            }
                            System.out.println("\n");
                            choices(conn, dbProps);
                        }
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }

            public static void exitTime () {
                System.out.println("See ya");
            }
        }